#include <iostream>
#include <unordered_set>
#include <string>
#include <random>
#include <chrono>
#include <boost/functional/hash.hpp>

using namespace std;

struct Point
{
    int x;
    int y;

    Point(int x, int y) : x(x), y(y)
    {

    }

    bool operator==(const Point& p2) const
    {
        return (x == p2.x) && (y == p2.y);
    }
};

namespace std
{
    template<>
    struct hash<Point>
    {
        size_t operator()(const Point& p) const
        {
            size_t seed = 0;
            //boost::hash_combine(seed, p.x);
            //boost::hash_combine(seed, p.y);
            //return seed;
            return p.x + p.y*1024;
            //auto hasher = hash<string>();
            //return hasher(to_string(p.x) + to_string(p.y));
        }
    };
}

int main()
{
    unordered_set<Point> pts;

    random_device rd;
    mt19937_64 gen(rd());
    uniform_int_distribution<> dist(-200, 200);

    auto start = chrono::high_resolution_clock::now();

    pts.reserve(70000);

    for (int i = 0 ; i < 100000 ; ++i)
        pts.emplace(dist(gen), dist(gen));

    auto end = chrono::high_resolution_clock::now();
    cout << "Creating set: ";
    cout << chrono::duration_cast<chrono::milliseconds>(end-start).count() << endl;

    pts.rehash(pts.size()*1.3);

    cout << "Set size = " << pts.size() << endl;
    cout << "Bucket count = " << pts.bucket_count() << endl;
    cout << "Bucket max_load_factor = " << pts.max_load_factor() << endl;
    cout << "Bucket load_factor = " << pts.load_factor() << endl;
    //sweepin throu buckets
    int empty_count = 0;
    int max_count = 0;
    for(int i = 0 ; i < pts.bucket_count() ; ++i)
    {
        auto s = pts.bucket_size(i);
        if (s == 0) ++empty_count;
        if (s > max_count) max_count = s;
    }
    cout << "Empty bucket count = " << empty_count << endl;
    cout << "Max bucket size = " << max_count << endl;

    start = chrono::high_resolution_clock::now();
    int counter;
    for (int i = 0 ; i < 100000 ; ++i)
    {
        Point p(dist(gen), dist(gen));
        if (pts.count(p) == 1) counter++;
    }
    end = chrono::high_resolution_clock::now();
    cout << "Testing set: ";
    cout << chrono::duration_cast<chrono::milliseconds>(end-start).count() << endl;
    cout << counter << " hits" << endl;

    return 0;
}

