#include <iostream>
#include <unordered_set>
#include <string>

using namespace std;

class Book
{
    int id_;
    string name_;
public:
    Book(int id, string name) : id_(id), name_(name)
    {
    }

    int get_id() const
    {
        return id_;
    }

    string get_name() const
    {
        return name_;
    }

    void print() const
    {
        cout << "id: " << id_ << " name: " << name_ << endl;
    }

    bool operator==(const Book& other) const
    {
        return other.id_ == id_;
    }
};

struct BookIdHasher
{
    size_t operator()(const Book& book) const
    {
        return book.get_id();
    }
};

struct BookNameHasher
{
    size_t operator()(const Book& book) const
    {
        auto hasher = std::hash<string>();
        return hasher(book.get_name());
    }
};

namespace std
{
    template<>
    struct hash<Book>
    {
        size_t operator()(const Book& book) const
        {
            auto hasher = std::hash<string>();
            return hasher(book.get_name());
        }
    };
}

int main()
{
    unordered_set<Book> s;
    s.emplace(1, "Swann's Way");
    s.emplace(2, "Summa Technlogiae");
    s.emplace(3, "Valis");

    for(auto& el : s)
        el.print();

    return 0;
}

