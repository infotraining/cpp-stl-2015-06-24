#include <vector>
#include <algorithm>
#include <iostream>
#include <iterator>
#include <functional>
#include <cmath>
#include <numeric>

using namespace std;

struct Stats
{
    double avg{};
    long sum{};
    long max{};
    int count{};
    void operator()(int x)
    {
        ++count;
        sum += x;
        avg = double(sum)/double(count);
        if (max < x) max = x;
    }

};

class RandGen
{
    int range_;
public:
    RandGen(int range) : range_(range)
    {
    }

    int operator()() const
    {
        return rand() % range_;
    }
};

template <typename Cont>
void print(Cont c)
{
    cout << "[ ";
    for (const auto& el : c)
        cout << el << ", ";
    cout << " ]" << endl;
}

int main()
{
    vector<int> vec(25);
    generate(vec.begin(), vec.end(), RandGen(30));

    print(vec);

    // 1a - wyświetl parzyste
    cout << "parzyste: ";
    copy_if(vec.begin(), vec.end(), ostream_iterator<int>(cout, ", "),
            [](int i) { return !(i % 2);});
    cout << endl;

    // 1b - wyswietl ile jest nieparzystych
    cout << "ile nieparzystych: ";
    cout << count_if(vec.begin(), vec.end(),
                     [](int i) { return i % 2;}) << endl;

    // 2 - usuń liczby podzielne przez dowolną liczbę z tablicy eliminators
    int eliminators[] = { 3, 5, 7 };

    auto elim = [&] (int i) { return any_of(begin(eliminators),
                                            end(eliminators),
                                            [i](int y) { return i%y ==0;}); };

    auto it_garbage = remove_if(vec.begin(), vec.end(), elim);
    print(vec);
    vec.erase(it_garbage, vec.end());
    print(vec);
    // 3 - tranformacja: podnieś liczby do kwadratu
    transform(vec.begin(), vec.end(), vec.begin(), [](int x) { return x*x;});
    print(vec);
    // 4 - wypisz 5 najwiekszych liczb
    nth_element(vec.begin(), vec.begin()+5, vec.end(), greater<int>());
    print(vec);
    cout << "piec najwiekszych: " ;
    copy(vec.begin(), vec.begin()+5, ostream_iterator<int>(cout, ", "));
    cout << endl;
    // 5 - policz wartosc srednia

    double sum{};
    for_each(vec.begin(), vec.end(), [&sum] (int x) { sum += x;});

    auto s = for_each(vec.begin(), vec.end(), Stats());
    cout << "avg = " << s.avg << endl;
    cout << "max = " << s.max << endl;
    cout << "sum = " << s.sum << endl;

    // cout << "avg = " << sum/double(vec.size()) << endl;

    // 6 - utwórz dwa kontenery - 1. z liczbami mniejszymi lub równymi średniej,
    //        2. z liczbami większymi od średniej
    vector<int> less_than_avg;
    vector<int> greater_than_avg;
//    partition_copy(vec.begin(), vec.end(),
//                   back_inserter(greater_than_avg),
//                   back_inserter(less_than_avg),
//                   [&s] (int x) { return x > s.avg;});
    for(auto& el : vec)
    {
        if(el > s.avg)
            greater_than_avg.push_back(el);
        else
            less_than_avg.push_back(el);
    }
    cout << "less: ";
    print(less_than_avg);
    cout << "greater: ";
    print(greater_than_avg);
}
