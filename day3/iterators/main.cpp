#include <iostream>
#include <vector>
#include <list>
#include <string>
#include <algorithm>
#include <iterator>

using namespace std;

int main()
{
    vector<int> src{1,2,3,4};
    vector<int> v;

    back_insert_iterator<vector<int>> bii(v);

    for(auto& el : src)
    {
        if (el % 2 == 0 )
            //bii = el;
            v.push_back(el);
    }

    copy_if(src.begin(), src.end(), bii,
            [] (int a) { return a%2 ==0;});


    for(auto& el : v)
        cout << el << ", ";
    cout << endl;

    // ---------------

    list<string> l1 = {"ala", "ma", "kota"};

    //vector<string> v1(l1.begin(), l1.end());
    //vector<string> v1(make_move_iterator(l1.begin()),
    //                 make_move_iterator(l1.end()));
    vector<string> v1;
    back_insert_iterator<vector<string>> bis(v1);
    move(l1.begin(), l1.end(), bis);

//    vector<string> v1;
//    for(auto& el : l1)
//    {
//        v1.push_back(move(el));
//    }

//    for(auto& el : l1)
//        cout << el << ", ";
//    cout << endl;
    copy(l1.begin(), l1.end(), ostream_iterator<string>(cout, ", "));
    cout << endl;

    for(auto& el : v1)
        cout << el << ", ";
    cout << endl;

    return 0;
}

