#include "person.hpp"

#include <iostream>
#include <vector>
#include <iterator>
#include <algorithm>
#include <functional>

using namespace std;

auto check_salary(double salary)
{
    return [salary] (const auto& p) { return p.salary() > salary;};
}

int main()
{
    vector<Person> employees;
    fill_person_container(employees);

    cout << "Wszyscy pracownicy:\n";
    copy(employees.begin(), employees.end(), ostream_iterator<Person>(cout, "\n"));
    cout << endl;

    // wyświetl pracownikow z pensją powyżej 3000
    int salary = 3000;
    copy_if(employees.begin(), employees.end(), ostream_iterator<Person>(cout, "\n"),
            //[&salary] (const Person& p) { return p.salary() > salary;});
            check_salary(3000));
    // wyświetl pracowników o wieku poniżej 30 lat
    // posortuj malejąco pracownikow wg nazwiska
    // wyświetl kobiety
    // ilość osob zarabiajacych powyżej średniej
}
