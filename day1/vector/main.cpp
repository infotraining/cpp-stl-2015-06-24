#include <iostream>
#include <vector>
#include <string>

using namespace std;

template<typename C>
void print(const C& c)
{
    cout << "[ ";
    for(auto& el : c)
        cout << el << ", ";
    cout << " ]" << endl;
}

auto main() -> int
{
    cout << "Hello vector" << endl;
    {
        vector<int> other_v(40, -10);
    }
    //********************
    auto a = 0; // 0
    int b = int();    // 0
    int c{}; // 0
    int d; // cout << d = undefined behaviour
    //********************

    vector<int> v(5, -1);
    //v.reserve(100);
    v.resize(100);
    cout << v[5] << endl;

    cout << "size: " << v.size() << " capacity: " << v.capacity() << endl;
    v.push_back(10);
    v.shrink_to_fit();
    cout << "size: " << v.size() << " capacity: " << v.capacity() << endl;
    print(v);

    auto h = {1,2,3,4,5};
    vector<int> v2{1,2,3,4,5};
    vector<string> v3{10, "b"};
    vector<int> v4(10,2);
    vector<int> v5{10,3};
    auto v6 = vector<int>(10);

    print(v2);
    print(v3);
    print(v4);
    print(v5);
    print(v6);

    for(auto& el : v2)
        el = el*2;

    for(const auto& el : v2)
        cout << el << endl;

    for(int i = 0 ; i < v2.size() ; ++i)
        cout << v2[i] << endl;

    cout << "*****************" << endl;
    vector<int>::const_iterator it = v5.begin();
    for(; it != v5.end() ; ++it)
        cout << *it << endl;
    cout << "*****************" << endl;

    for(auto it = v2.cbegin() ; it != v2.cend() ; ++it)
        cout << *it << endl;

    int v10[3] = {1,2,3};
    for(auto it = begin(v10) ; it != end(v10) ; ++it)
        cout << *it << endl;



    return 0;
}

