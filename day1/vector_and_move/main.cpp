#include <iostream>
#include <vector>
#include <memory>

using namespace std;

class Gadget
{
    int id_;
public:
    void print()
    {
        cout << "Gadget " << id_ << endl;
    }

    Gadget(int id) : id_(id)
    {
        cout << "ctor " << id_ << endl;
    }

    ~Gadget()
    {
        cout << "dtor " << id_ << endl;
    }

//    Gadget(const Gadget& other)
//    {
//        id_ = other.id_;
//        cout << "copy ctor " << id_ << endl;
//    }

    Gadget(Gadget&& tother)
    {
        id_ = move(tother.id_);
        cout << "move ctor " << id_ << endl;
    }

    Gadget& operator=(Gadget&& other)
    {
        id_ = move(other.id_);
        cout << "move assignement " << id_ << endl;
    }

//    Gadget& operator=(const Gadget& other)
//    {
//        id_ = other.id_;
//        cout << "copy assignement" << id_ << endl;
//    }
};

Gadget make_gadget(int id)
{
    return Gadget(id);
}

void sink(Gadget g)
{
    g.print();
}

int main()
{
    cout << "Hello World!" << endl;
    //vector<unique_ptr<Gadget>> v;
//    Gadget g(100);
//    sink(move(g));
    vector<Gadget> v;
    //Gadget g(Gadget(10));

    //v.reserve(10);
    //v.push_back(make_unique<Gadget>(1));
    //v.push_back(make_unique<Gadget>(2));
    v.emplace_back(11);
    v.push_back(Gadget(10));
    v.emplace_back(make_gadget(20));
    //v.shrink_to_fit();
    cout << "*********************" << endl;
    return 0;
}

