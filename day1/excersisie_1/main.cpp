#include <iostream>
#include <cstdlib>
#include <vector>
#include <algorithm>
#include <cassert>
#include <list>
#include <functional>
#include <iterator>

using namespace std;

template <typename Cont>
void print(Cont c)
{
    cout << "[ ";
    for (const auto& el : c)
        cout << el << ", ";
    cout << " ]" << endl;
}


void avg(int array[], size_t size)
{
    double sum = 0.0;
    for(size_t i = 0; i < size; ++i)
        sum += array[i];

    cout << "AVG: " << sum / size << endl;
}

bool is_even(int n)
{
    return n % 2 == 0;
}

int main()
{
    int tab[100];

    for(int i = 0; i < 100; ++i)
        tab[i] = rand() % 100;

    // 1 - utwórz wektor vec_int zawierający kopie wszystkich elementów z  tablicy tab
    //vector<int> vec_int(tab, tab+100);
    vector<int> vec_int(begin(tab), end(tab)); // c++11
    print(vec_int);
    // 2 - wypisz wartość średnią przechowywanych liczb w vec_int
    avg(vec_int.data(), vec_int.size());

    // 3a - utwórz kopię wektora vec_int o nazwie vec_cloned
    vector<int> vec_cloned(move(vec_int));
    //vector<int> vec_cloned(vec_int);

    // 3b - wyczysć kontener vec_int i zwolnij pamięć po buforze wektora
    //vec_int = vector<int>();
    //vec_int.shrink_to_fit();
    cout << vec_int.size() << ", " << vec_int.capacity() << endl;

    int rest[] = {1,2,3,4,5};
    // 4 - dodaj na koniec wektora vec_cloned zawartość tablicy rest

    vec_cloned.insert(vec_cloned.end(), begin(rest), end(rest));

    for(auto& el : rest)
        vec_cloned.push_back(el);

    print(vec_cloned);
    // 5 - posortuj zawartość wektora vec_cloned
    sort(begin(vec_cloned), end(vec_cloned));
    print(vec_cloned);

    // 6 - wyświetl na ekranie zawartość vec_cloned za pomocą iteratorów

    // 7 - utwórz kontener numbers (wybierz odpowiedni typ biorąc pod uwagę następne polecenia) zawierający kopie elementów vec_cloned
    list<int> numbers(vec_cloned.cbegin(), vec_cloned.cend());
    // 8 - usuń duplikaty elementów przechowywanych w kontenerze
    cout << " ---- list Numbers ----" << endl;
    print(numbers);
    // numbers.sort(); // not needed rigth now
    numbers.unique();
    print(numbers);
    // 9 - usuń liczby parzyste z kontenera
    // numbers.remove_if(is_even);
    numbers.remove_if([] (int n) { return !(n % 2);});
    print(numbers);
    // 10 - wyświetl elementy kontenera w odwrotnej kolejności
    for(auto it = numbers.crbegin() ; it != numbers.crend() ; ++it)
        cout << *it << ", ";
    cout << endl;
    // 11 - skopiuj dane z numbers do listy lst_numbers jednocześnie i wyczyść kontener numbers
    //list<int> lst_numbers(move(numbers));
    list<int> lst_numbers;
    lst_numbers.splice(lst_numbers.begin(), numbers);
    print(lst_numbers);
    print(numbers);
    // 12 - wyświetl elementy kontenera lst_numbers w odwrotnej kolejności
    lst_numbers.reverse();
    print(lst_numbers);
}

