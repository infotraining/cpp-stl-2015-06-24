#include <iostream>
#include <vector>

using namespace std;

int main()
{
    vector<bool> v;
    for (int i = 0 ; i < 10 ;++i)
        v.push_back(true);

    cout << v[2] << endl;
    for(auto&& el : v)
        el = false;

    for(auto&& el : v)
        cout << el << ", ";
    cout << endl;
    return 0;
}

