#include <iostream>
#include <array>


using namespace std;

template<typename C>
void print(const C& c)
{
    cout << "[ ";
    for(auto& el : c)
        cout << el << ", ";
    cout << " ]" << endl;
}

int main()
{
    cout << "Hello array!" << endl;
    array<int, 8> arr;
    cout << sizeof(arr) << endl;
    print(arr);
    cout << std::get<8>(arr) << endl;
    cout << arr.at(7) << endl;
    return 0;
}

