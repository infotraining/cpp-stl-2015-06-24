#include <iostream>
#include <map>
#include <unordered_map>
#include <fstream>
#include <string>
#include <boost/algorithm/string.hpp>
#include <functional>
#include <chrono>

using namespace std;

int main()
{
    cout << "concordance for M.Proust Swanns Way" << endl;

    ifstream inpf("proust.txt");
    if(!inpf)
    {
        cerr << "error accessing file" << endl;
        return -1;
    }

    unordered_map<string, int> counter;
    string word;

    while(inpf >> word)
    {
        boost::to_lower(word);
        boost::trim_if(word, boost::is_any_of("\"'\t?!,.;:()-/\\"));
        counter[move(word)]++;
    }

    // debug
    //for(const auto& el : counter)
    //    cout << el.first << ": " << el.second << endl;

    /* map
    map<int, vector<string>, greater<int>> results;
    for(const auto& el : counter)
        results[el.second].push_back(el.first);

    int c{};
    for(const auto& el : results)
    {
        if (c == 20) break;
        cout << el.first << ": ";
        for(const auto& word : el.second)
            cout << word << ", ";
        cout << endl;
        ++c;
    }*/

    // multimap
    multimap<int, string> results;
    for(const auto& el : counter)
        results.emplace(el.second, el.first);

    for(auto iter = results.begin() ; iter != results.end() ;)
    {
        //cout << el.first << ": " << el.second << endl;
        cout << iter->first << ": ";
        auto iter_pair = results.equal_range(iter->first);
        for(auto it = iter_pair.first ; it != iter_pair.second ; ++it)
        {
            cout << it->second << ", ";
        }
        cout << endl;
        iter = iter_pair.second;
    }

    return 0;
}

