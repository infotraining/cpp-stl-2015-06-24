#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <set>
#include <unordered_set>
#include <vector>
#include <chrono>
#include <algorithm>
#include <iterator>

using namespace std;

int main()
{    
    string text = "this is a snetence without meaning this is a snetence without meaning this is a snetence without meaning this is a snetence without meaning";
    vector<string> words;
    string word;
    istringstream str(text);
    while (str >> word)
        words.push_back(word);

    // preparing dictionary


    //vector<string> dict;
    ifstream inp("en.dict");
    if(!inp)
    {
        cerr << "error accessing dict file" << endl;
        return -1;
    }


    auto start = chrono::high_resolution_clock::now();

    istream_iterator<string> dic_begin(inp);
    istream_iterator<string> dic_end;
    unordered_set<string> dict(dic_begin, dic_end);
//    while(inp >> word)
//        dict.emplace_hint(dict.end(), word);
//        //dict.push_back(move(word));

    auto end = chrono::high_resolution_clock::now();
    cout << "Creating dict: ";
    cout << chrono::duration_cast<chrono::milliseconds>(end-start).count() << endl;

    // preparing vector of words
    cout << "Words: ";
//    for(const auto& el : words)
//        cout << el << ", ";
    cout << endl;

    // checking
    start = chrono::high_resolution_clock::now();

    cout << "--------------" << endl;
    int misspelled_count = 0;
    //sort(dict.begin(), dict.end()); // vector only
    for(int i = 0 ; i < 100 ; ++i)
    {
        for(const auto& el : words)
        {
            if (dict.count(el) == 0)
            //if (binary_search(dict.begin(), dict.end(), el) == false)
                ++misspelled_count;
        }
    }
    cout << "bad words count = " << misspelled_count << endl;
    end = chrono::high_resolution_clock::now();
    cout << "Checking dict: ";
    cout << chrono::duration_cast<chrono::milliseconds>(end-start).count() << endl;

    return 0;
}

