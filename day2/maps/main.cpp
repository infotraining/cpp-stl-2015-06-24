#include <iostream>
#include <map>

using namespace std;

int main()
{
    map<string, double> stocks;
    stocks.insert(pair<string, double>("GOGL", 12.1));
    stocks.insert(make_pair("APPL", 1.1));
    stocks.emplace("NKIA", 5.0);

    stocks["NKIA"] = 8.0;
    stocks["IBM"] = .3;

    cout << stocks["NOKIA"] << endl;

    for(auto& el : stocks)
        cout << el.first << ": " << el.second << endl;


    return 0;
}

