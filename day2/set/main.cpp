#include <iostream>
#include <set>
#include <functional>

using namespace std;

class Gadget
{

public:
    int id_;
    void print() const
    {
        cout << "Gadget " << id_ << endl;
    }

    Gadget(int id) : id_(id)
    {
        cout << "ctor " << id_ << endl;
    }

    ~Gadget()
    {
        cout << "dtor " << id_ << endl;
    }

//    Gadget(const Gadget& other)
//    {
//        id_ = other.id_;
//        cout << "copy ctor " << id_ << endl;
//    }

    Gadget(Gadget&& tother)
    {
        id_ = move(tother.id_);
        cout << "move ctor " << id_ << endl;
    }

    Gadget& operator=(Gadget&& other)
    {
        id_ = move(other.id_);
        cout << "move assignement " << id_ << endl;
    }

//    Gadget& operator=(const Gadget& other)
//    {
//        id_ = other.id_;
//        cout << "copy assignement" << id_ << endl;
//    }
};

int main()
{
    cout << "Hello set!" << endl;
    set<int, greater<int>> s1;

    s1.insert(10);
    s1.insert(5);
    s1.insert(6);
    s1.insert(1);

    //auto res = s1.insert(6);
    if(s1.insert(3).second)
        cout << "inserted" << endl;
    else
        cout << "not inserted" << endl;

    cout << "Has got 10? " << s1.count(10) << endl;


    for (auto& el : s1)
        cout << el << ", ";
    cout << endl;


//    set<Gadget, std::function<bool(const Gadget&, const Gadget&)>>
//        s2([](const Gadget& g1, const Gadget& g2){return g1.id_ < g2.id_;});

    auto op = [](const Gadget& g1, const Gadget& g2){return g1.id_ < g2.id_;};

    set<Gadget, decltype(op)> s2(op);
    //set<Gadget, bool(*)(const Gadget&, const Gadget&)> s2(op);
    s2.emplace(1);

    for (auto& el : s2)
        el.print();
    cout << endl;

    return 0;
}

