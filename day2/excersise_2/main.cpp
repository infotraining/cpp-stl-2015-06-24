#include <iostream>
#include <vector>
#include <deque>
#include <list>
#include <ctime>
#include <algorithm>
#include <cstring>
#include <chrono>
#include <random>

using namespace std;

// szablony funkcji umożliwiające sortowanie kontenera
class ExpensiveObject
{
private:
    char* data_;
    size_t size_;
public:
    ExpensiveObject(const char* data) : data_(NULL), size_(std::strlen(data))
    {
        data_ = new char[size_+1];
        std::strcpy(data_, data);
    }

    // copy semantic
    ExpensiveObject(const ExpensiveObject& source) : data_(NULL), size_(source.size_)
    {
        data_ = new char[size_+1];
        std::strcpy(data_, source.data_);
    }

    ExpensiveObject& operator=(const ExpensiveObject& source)
    {
        ExpensiveObject temp(source);
        swap(temp);
        return *this;
    }

    // move semantic

    ExpensiveObject(ExpensiveObject&& source) noexcept
    {
        data_ = source.data_;
        size_ = source.size_;
        source.data_ = nullptr;
        source.size_ = 0;
    }

    ExpensiveObject& operator=(ExpensiveObject&& source) noexcept
    {
        ExpensiveObject temp(source);
        swap(temp);
        return *this;
    }

    ~ExpensiveObject()
    {
        delete [] data_;
    }

    void swap(ExpensiveObject& other)
    {
        std::swap(data_, other.data_);
        std::swap(size_, other.size_);
    }

    const char* data() const
    {
        return data_;
    }

    size_t size() const
    {
        return size_;
    }

    bool operator<(const ExpensiveObject& other) const
    {
        if (std::strcmp(data_, other.data_) < 0)
            return true;
        return false;
    }
};

ExpensiveObject expensive_object_generator()
{
    static char txt[] = "abcdefghijklmn";
    static const size_t size = std::strlen(txt);

    std::random_shuffle(txt, txt + size);

    return ExpensiveObject(txt);
}

template<typename Cont>
void sort(Cont& cont)
{
    sort(cont.begin(), cont.end());
}

template<typename T>
void sort(list<T> cont)
{
    cont.sort();
}

template<typename Cont>
void test(Cont& cont, long size)
{
    auto start = chrono::high_resolution_clock::now();
    for(int i = 0 ; i < size ; ++i)
        cont.push_back(expensive_object_generator());

    auto end = chrono::high_resolution_clock::now();
    cout << "Creating: ";
    cout << chrono::duration_cast<chrono::microseconds>(end-start).count() << endl;

    //random_shuffle(cont.begin(), cont.end());
    start = chrono::high_resolution_clock::now();

    sort(cont);

    end = chrono::high_resolution_clock::now();
    cout << "Sorting: ";
    cout << chrono::duration_cast<chrono::microseconds>(end-start).count() << endl;
}

int main()
{
/*
    Utwórz trzy sekwencje typu int: vector, deque i list. Wypelnij je wartosciami losowymi.
    Porównaj czasy tworzenia i wypelniania danymi sekwencji. Napisz szablon funkcji sortującej sekwencje
    vector i deque. Napisz specjalizowany szablon funkcji realizującej sortowanie dla kontenera list.
    Porównaj efektywność operacji sortowania.
*/

    for(int SIZE = 10000 ; SIZE < 100000001 ; SIZE*= 10)
    {
        vector<ExpensiveObject> cont;
        cont.reserve(SIZE);
        cout << "Vector " << SIZE << endl;
        test(cont, SIZE);

        deque<ExpensiveObject> dq;
        cout << "Deque " << SIZE<< endl;
        test(dq, SIZE);

        list<ExpensiveObject> l;
        cout << "List " << SIZE << endl;
        test(l, SIZE);
        cout << "-----------------------" << endl;
    }

}
